import React from "react";
import "./Card.css"

export class Card extends React.Component{


    render() {
        let { id,title,price,description,category,image,rating} = this.props.products
        
        return(
            <div className="card-container" card-id={id}>
                <div className="card-title">{title}</div>
                <div className="card-img">
                    <img src={image} alt="img" />
                </div>
                <div className="card-properties">
                    <div className="description">{description}</div>
                    <div className="price">{price}$</div>
                    <button>Купити</button>
                    <div className="rating">
                        <div className="category">{category}</div>
                        <div className="rate">💟{rating.rate}/{rating.count}</div>
                    </div>
                </div>
            </div>
        )
    }
}