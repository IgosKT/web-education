import React from "react";
import { Card } from "../Card/Card.jsx"
import { nanoid } from "nanoid";

import "./App.css";

export class App extends React.Component {
	state = {
		products: [],
	};

	componentDidMount() {
		this.getData("https://fakestoreapi.com/products");
	}

	getData = (url) => {
		let xhr = new XMLHttpRequest();
		xhr.open("get", url);
		xhr.onreadystatechange = () => {
			if (xhr.readyState === 4 && xhr.status === 200) {
				let data = JSON.parse(xhr.response);
				this.setState({
					products: data,
				});
			}
		};
		xhr.send();
	};

	render() {
		const products = this.state.products
		console.log(products)
		return (
			<div className="container">
				{products.map((el,i) => {
					return <Card products={el} key={nanoid()} /> 
				})}
			</div>
		)
	}
}