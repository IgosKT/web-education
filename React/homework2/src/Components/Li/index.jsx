import React from "react";
import { Img } from "../Img";
import { Content } from "../Content";


export function Li({imgClass,imgSrc,imgAlt,className,value,href}) {
    return (
			<>
				<li>
					<Img className={imgClass} src={imgSrc} alt={imgAlt} />
					<Content className={className} value={value} href={href} />
				</li>
			</>
		);
}