import React from "react";
import { Search } from "../Search";
import { Li } from "../Li";
import { Input } from "../Input";
import { Img } from "../Img";

import dashboard from "../../icons/dashboard.svg";
import arrowRight from "../../icons/arrowRight.svg";
import revenue from "../../icons/revenue.svg";
import notification from "../../icons/notification.svg";
import analytics from "../../icons/analytics.svg";
import inventory from "../../icons/inventory.svg";
import logout from "../../icons/logout.svg";
import mode from "../../icons/mode.svg";
import moon from "../../icons/moon.svg";

import "./app.css";

function App() {
    return (
        <>
            <div className="navigation-container">
                <div className="user-container">
                    <div className="user-avatar">AF</div>
                    <div className="user-data">
                        <div className="user-name text">AnimatedFred</div>
                        <div className="user-email text">animated@demo.com</div>
                    </div>
                    <div className="user-arrow"><img src={arrowRight} alt=""/></div>
                </div>
                <div className="search">
                    <Search type="search" placeholder="Search..."/>
                </div>
                <ul className="navigation-menu">  
                    <Li imgSrc={dashboard} imgAlt="dashboard" className="text" value="Dashboard" href="/"/>
                    <Li imgSrc={revenue} imgAlt="revenue" className="text" value="Revenue" href="/"/>
                    <Li imgSrc={notification} imgAlt="notification" className="text" value="Notification" href="/" />
                    <Li imgSrc={analytics} imgAlt="analytics" className="text" value="Analytics" href="/" />
                    <Li imgSrc={inventory} imgAlt="inventory" className="text" value="Inventory" href="/"/>
                </ul>
                <ul className="logout-menu">
                    <Li imgSrc={logout} imgAlt="logout" className="text" value="Logout" href="/"/>
                    <Li imgClass="sun" imgSrc={mode} imgAlt="mode" className="text" value="Light mode" href="/"/>
                    <label className="mode" >
                        <Input className="switch" type="checkbox" />
                        <Img src={moon} alt="moon" />
                    </label>
                </ul>
            </div>
        </>
    )
};

export default App;
