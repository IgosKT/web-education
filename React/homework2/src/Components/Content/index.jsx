import React from "react";


export function Content({className,value,href}) {
    return (
        <>
            <span className={className}><a href={href}>{value}</a></span>
        </>
    )
}