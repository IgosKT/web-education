import React from "react";
import { Img } from "../Img";
import { Input } from "../Input";

import glass from "../../icons/glass.svg";

export function Search(options) {
    return (
        <>
            <Img className="search-btn" src={glass} alt="glass" />
            <Input type={options.type} placeholder={options.placeholder} />
        </>
    )
}


