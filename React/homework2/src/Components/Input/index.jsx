import React from "react";

export function Input({ className, type, placeholder}) {
    return (
        <>  
            <input className={className} type={type} placeholder={placeholder} />
        </>
    )
}