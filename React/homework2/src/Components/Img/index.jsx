import React from "react";

export function Img({className,src,alt}) {
    return (
        <>
            <div className={className}>
                <img src={src} alt={alt} />
            </div>
        </>
    )
}