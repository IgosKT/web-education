import React from "react";

import { Button } from "../Button";
import { ToDoItem } from "../ToDoItem";

import "./app.css"

export class App extends React.Component{
    
    constructor() {
        super();
        this.id = 100;
        this.taskInput = React.createRef(); // створення посилання 
        this.state = {
            openFormButton: {
                className: "show",
            },
            input: {
                className: "hide"
            },
            addButton: {
                className: "hide",
            },
            tasks:[]
        }
    }

    // відображення форми
    showInput = () => {
        this.setState((state)=>{
            return {
                openFormButton: {
                    className: "hide",
                },
                input: {
                    className: "show"
                },
                addButton: {
                    className: "show",
                }
            }
        })
    }
    //створення об'єкта задачі
    addTask = () => {
        let taskElement = {
            taskId: ++this.id,
            value: this.taskInput.current.value,
            className: "taskText",
            done: false,
            readOnly: true,
            toDelete :  (id) => {
                this.setState(({ tasks }) => {
                    return {
                        tasks : tasks.filter((el) => el.taskId !== id)
                    }
                })
            }
        }
       
        this.setState(({tasks}) => tasks.push(taskElement))
    }  
    
    render() {
        return (
            <>
                <Button value="додати ➕" className={"openFormButton " + this.state.openFormButton.className} clickFN={this.showInput} />
                <div className="form-add">
                    <input type="text" className={"taskInput " + this.state.input.className} placeholder="add yor task" ref={this.taskInput} />
                    <Button value="зберегти 📀" className={"addTask " + this.state.addButton.className} clickFN={this.addTask}/>
                </div>
                <div className="form-tasks">
                    {this.state.tasks.map((el) => {
                        return <ToDoItem taskData={el} remove={this.toDelete} key={el.taskId} />
                    })}
                </div>
            </>
        )
    }
}


/* addTask = () => {
    let taskElement = {
        taskId: ++this.id,
        value: this.taskInput.current.value,
        className: "taskText",
        done: false,
        readOnly: true,
        toDone: this.taskToDone
    }
   
    this.setState(({tasks}) =>  tasks.push(taskElement))
} */
// зміна статусу виконання, складніший варіант
/* taskToDone = (evt) => {
    let findId = this.state.tasks.findIndex(el => el.taskId === Number(evt.target.id))
    this.setState(({ tasks }) => tasks[findId].done = true)
} */

//let findId = this.state.tasks.findIndex((el) => el.taskId === Number(evt.target.id));
//let newArr = [...this.state.tasks].filter((el) => el.taskId !== Number(evt.target.id));

/* toDelete = (evt) => {
    this.setState((state) => {
        return {
            ...state,
            tasks : [...state.tasks].filter((el) => el.taskId !== Number(evt.target.id)),
        }
    })
} */


/* let findId = this.state.tasks.findIndex((el) => el.taskId === id);
tasks.splice(findId, 1);
const newArray = [...tasks.slice(0, findId), ...tasks.slice(findId + 1)];  */