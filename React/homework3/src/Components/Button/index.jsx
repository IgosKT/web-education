export const Button = ({value,className,clickFN}) => {
    return (
        <button className={className} onClick={clickFN}>{ value }</button>   
    )
}