import { useState } from "react";

export const ToDoItem = ({ taskData,remove}) => {
    let [done, toDone] = useState(taskData.done);// зміна статусу виконання
    let [readOnly, toChange] = useState(taskData.readOnly); // коригування задачі
    
    let className = taskData.className;
    if (done) className = taskData.className + " done"; 

    return (
        <div className="task-to-do">
            <input
                type="text" defaultValue={taskData.value}
                className={className} id={taskData.taskId}
                readOnly={readOnly}
                onBlur={() => { toChange(true) }}
                onMouseDown={(e) => { e.preventDefault() }}
            />
            <label className="changeTask" htmlFor={taskData.taskId}>
                <span id={taskData.taskId} onClick={()=>{toChange(readOnly=false)}}>✍️</span>
                <span id={taskData.taskId} onClick={()=>{taskData.toDelete(taskData.taskId)}}>❌</span>
                <span id={taskData.taskId} onClick={()=>{toDone(done=true)}}>✅</span>
            </label>
        </div>
    )
}

/* ()=>{toChange(false)} */