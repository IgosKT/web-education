/* class Car {
    constructor (model,carClass,weight,driver,engine) {
        this.model = model,
        this.carClass = carClass,
        this.weight = weight,
        this.driver = driver,
        this.engine = engine;
    }

    start () {
        console.log('Поїхали')
    }

    stop () {
        console.log('Зупиняємося')
    }

    turnRight () {
        console.log('Поворот праворуч')
    }

    turnLeft () {
        console.log('Поворот ліворуч')
    }

    toString () {
        console.dir(this)
    }
};

class Engine extends Car {
    constructor (power,company) {
        super()
        this.power = power,
        this.company = company;
    }

    toCar () {
        return `Виробник - ${this.company}, Потужність - ${this.power}`
    }
}

class Driver extends Engine {
    constructor (fname,exp) {
        super()
        this.fullName = fname,
        this.experiance = exp;
    }

    toCar () {
        return `Водій - ${this.fullName}, Досвід - ${this.experiance}`
    }
}


const driver = new Driver('Ілон Маск','20 років');
const engine = new Engine('300kW','Tesla');
const car = new Car ('Tesla','Passenger','1200kg',driver.toCar(),engine.toCar()); // СМУЩАЄ ТАКИЙ ВАРІАНТ
car.toString(); */

//-----------------------------------------------------------------------------------------------

class Driver {
  constructor(fname, exp) {
    (this.fullName = fname),
    (this.experiance = exp);
  }
  toCar() {
    return `Водій - ${this.fullName}, Досвід - ${this.experiance}`;
  }
}

class Engine extends Driver {
  constructor(power, company, fname, exp) {
    super(fname, exp);
    (this.power = power),
    (this.company = company);
  }
  toCar() {
    return `Виробник - ${this.company}, Потужність - ${this.power}`;
  }
}

class Car extends Engine {
  constructor(model, carClass, weight, power, company, fname, exp) {
    super(power, company, fname, exp);
    (this.model = model),
    (this.carClass = carClass),
    (this.weight = weight);
  }

  start() {
    console.log("Поїхали");
  }

  stop() {
    console.log("Зупиняємося");
  }

  turnRight() {
    console.log("Поворот праворуч");
  }

  turnLeft() {
    console.log("Поворот ліворуч");
  }

  toString() {
    console.dir(this);
  }
}

const car = new Car(
  "Tesla",
  "Passenger",
  "1200kg",
  "300kW",
  "Tesla",
  "Ілон Маск",
  "20 років"
);
car.toString();

//----------------------------------------------------------------------------------------------------
class Lorry extends Car {
  constructor(lcap, ...rest) {
    super(...rest);
    this.loadCapacity = lcap;
  }
}
const lorry = new Lorry(
  "800kg",
  "Мерседес",
  "Вантажівка",
  "550кг",
  "275kW",
  "Мерседес",
  "Ерл",
  "15 років"
);
lorry.toString();

//-----------------------------------------------------------------------------------------------------
class SportCar extends Car {
  constructor(mspeed, ...rest) {
    super(...rest);
    this.maxSpeed = mspeed;
  }
}
const sportcar = new SportCar(
  "467m/h",
  "Ферарі",
  "Спорткар",
  "450кг",
  "375kW",
  "Ферарі",
  "Михайло Шумахер",
  "25 років"
);
sportcar.toString();

//-----------------------------------------------------------------------------------------------------------

/* const Person = function () {
    let name = ''
  
    const getName = function () {
      return name
    }
  
    const setName = function (val) {
      name = val
    }
  
    return {
      setName, getName
    }
  }  */
