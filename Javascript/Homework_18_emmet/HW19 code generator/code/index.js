import { nestingWithoutTag, linearCodeGen, nestingWithAttribute, nestingWithoutAttribute } from "./functions.js";

export const eneterField = document.getElementsByTagName('textarea')[0];
export let section = document.getElementsByTagName('section')[0];

let wordsPattern = /[a-z]+|\d+| |-|_{1,2}|!|>|.|#/gi;
let nestingSigns = [];
let res;

export let neededArr = [];

eneterField.addEventListener('input', (e) => {
	section.textContent = '';
	neededArr = [];
	let signCount = [];
	while ((res = wordsPattern.exec(eneterField.value)) !== null) {
		neededArr.push(res[0]);
	}
     
    neededArr.forEach((el)=>{
	if(el === '>' || el === '^') {
	signCount.push(el)
	}
    })
	 if(signCount.length > 2) {
		console.error('Ну це вже занадто! Давай обмежимося двома вкладеностями. Добре?')
	 }
     
    console.log(signCount)

	if (eneterField.value.includes('>') && eneterField.value.includes('^')) {
		nestingWithoutAttribute(neededArr);
		nestingWithoutTag(neededArr)
		nestingWithAttribute(neededArr, '#', 'id');
		nestingWithAttribute(neededArr, '.', 'class');
		let stringToUp = neededArr.slice(neededArr.indexOf('^') + 1)
		linearCodeGen(stringToUp)
	}
	if (eneterField.value.includes('>') && !eneterField.value.includes('^')) {
		neededArr.forEach((el) => {
			if (el === '>') {
				nestingSigns.push(el);
			}
		})
	}
	if (nestingSigns.length === 2) {
		let splittedEnterField = eneterField.value.split('>');
		let slicedSplittedEnterField = splittedEnterField.slice(splittedEnterField.length - 2);
		slicedSplittedEnterField.splice(1, 0, '>')
		let joinedEnterField = slicedSplittedEnterField.join('');
		let nestingRes;
		let nestingNeededArr = [];
		while ((nestingRes = wordsPattern.exec(joinedEnterField)) !== null) {
			nestingNeededArr.push(nestingRes[0]);
		}
		nestingWithoutAttribute(nestingNeededArr);
		nestingWithoutTag(nestingNeededArr, '#', 'id' )
		nestingWithoutTag(nestingNeededArr )
		nestingWithAttribute(nestingNeededArr, '#', 'id');
		nestingWithAttribute(nestingNeededArr);
		let newString = `${splittedEnterField[0]}{${section.textContent}}`
		section.textContent = '';
		let textRes;
		let textArr = [];
		while ((textRes = wordsPattern.exec(newString)) !== null) {
			textArr.push(textRes[0]);
		}
		linearCodeGen(textArr);
	} else if (nestingSigns.length === 1 && !eneterField.value.includes('^')) {
		nestingWithAttribute(neededArr, '#', 'id');
		nestingWithAttribute(neededArr, '.', 'class');
		nestingWithoutAttribute(neededArr);
		nestingWithoutTag(nestingNeededArr, '#', 'id' )
		nestingWithoutTag(nestingNeededArr )
		
	} else if (!eneterField.value.includes('>') && !eneterField.value.includes('^')) {
		linearCodeGen(neededArr);
	}
})


// a>p>div
