import { section } from "./index.js";
export const validation = (p, v) => p.test(v);

export const arraySlicing = (arr) => {
	debugger
	let nestedArr = [];
	arr.map((elem) => {
		if (elem !== '>' && elem !== '^') {
			nestedArr.push(elem)
		} else {
			return;

		}
	})
	return nestedArr
}


export const linearCodeGen = (arr) => { 
	arr.forEach((el, i) => {
        // 		якщо є клас, але немає тегу
		if(el === '.'
		 && (!arr[i-1] || !validation(/[a-z]+/, arr[i-1]))
		 && arr[i+2] !== '{'
		 && arr[i-1] !== '>'
		 && arr[i+2] !== '>'
		 ){
			section.insertAdjacentText('beforeend', `<div class ="${arr[i + 1]}"></div>`);
		}

// 		якщо є id, але немає тегу
        if(el === '#'
		&& (!arr[i-1] || !validation(/[a-z]+/, arr[i-1]))
		&& arr[i+2] !== '{'
		&& arr[i-1] !== '>'
		&& arr[i+2] !== '>'
		){
			section.insertAdjacentText('beforeend', `<div id ="${arr[i + 1]}"></div>`);
		}

	        	//		*** ВСТАВЛЯЄМО ТЕКСТ ***

		if(el === '{') {
			let text = [];
			let secondBracket = arr.indexOf('}');
			text.push(arr.slice(i+1, secondBracket));

			// 		без тегу, але з атрибутом
            if((!arr[i-3] || arr[i-3] === '>')
           		 && arr[i-2] === '.' 
	       		 && validation(/[A-z]+/g, arr[i-1])
				 && (!arr[secondBracket+1] || arr[secondBracket+1] !== '*') ){
				section.insertAdjacentText('beforeend', `<div class="${arr[i-1]}">${text[0].join('')}</div>`);

			} else if( (!arr[i-3] || arr[i-3] === '>') 
			             && arr[i-2] === '#' 
						 && validation(/[A-z]+/g, arr[i-1])
						 && (!arr[secondBracket+1] || arr[secondBracket+1] !== '*')) {
				section.insertAdjacentText('beforeend', `<div id="${arr[i-1]}">${text[0].join('')}</div>`);
			} 
			
			// 		текст з тегом без атрибутів
			else if((!arr[i-2] || (!validation(/[A-z]+/g, arr[i-2])))
     				 && arr[i-1] 
	 				 && arr[i-2] !== '.' 
	  				 && arr[i-2] !== '#'
					 && (!arr[secondBracket+1] || arr[secondBracket+1] !== '*')){
				section.insertAdjacentText('beforeend', `<${arr[i-1]}>${text[0].join('')}</${arr[i-1]}>`);
			}

			//     без тега, множення
			else if( (!arr[i-3] || arr[i-3] === '>')
					   && arr[i-2] === '.' 
    				   && validation(/[A-z]+/g, arr[i-1]) 
					   && arr[secondBracket+1] === '*' 
					   && validation(/\d+/, arr[secondBracket+2])){
						for (let j = 0; j < arr[secondBracket+2]; j++) {
   							section.insertAdjacentText('beforeend', `<div class="${arr[i-1]}">${text[0].join('')}</div>`);
   							section.insertAdjacentHTML("beforeend", '</br>')
						}
			}
			else if( (!arr[i-3] || arr[i-3] === '>')
					   && arr[i-2] === '#' 
					   && validation(/[A-z]+/g, arr[i-1]) 
					   && arr[secondBracket+1] === '*' 
					   && validation(/\d+/, arr[secondBracket+2])){
						for (let j = 0; j < arr[secondBracket+2]; j++) {
  							section.insertAdjacentText('beforeend', `<div id="${arr[i-1]}">${text[0].join('')}</div>`);
   							section.insertAdjacentHTML("beforeend", '</br>')
						}
			}
			// 		текст з тегом без атрибутів, множення
			else if(!arr[i-3] || (!validation(/[A-z]+/g, arr[i-3])  
				     && arr[i-2] 
  					 && arr[i-2] !== '.'
  					 && arr[i-2] !== '#')
  					 && arr[secondBracket+1] === '*' 
 					 && validation(/\d+/, arr[secondBracket+2])){
					   for (let j = 0; j < arr[secondBracket+2]; j++) {
							section.insertAdjacentText('beforeend', `<${arr[i-1]}>${text[0].join('')}</${arr[i-1]}>`);
							section.insertAdjacentHTML("beforeend", '</br>')
	  					 }
			}
			//      текст з тегом і з атрибутом
			else if (arr[i-3] 
				     && validation(/[A-z]+/g, arr[i-3]) 
				     && arr[i-2] === '.' 
				     && validation(/[A-z]+/g, arr[i-1])
					 && arr[secondBracket + 1] !== '*') {
		             section.insertAdjacentText('beforeend', `<${arr[i-3]} class='${arr[i-1]}'>${text[0].join('')}</${arr[i-3]}>`);
	        } 

			else if (arr[i-3] 
				&& validation(/[A-z]+/g, arr[i-3]) 
				&& arr[i-2] === '#' 
				&& validation(/[A-z]+/g, arr[i-1])
				&& arr[secondBracket + 1] !== '*') {
			   section.insertAdjacentText('beforeend', `<${arr[i-3]} id='${arr[i-1]}'>${text[0].join('')}</${arr[i-3]}>`);

	        }
			//      текст з тегом і з атрибутом множення
			else if (arr[i-3] 
				&& validation(/[A-z]+/g, arr[i-3]) 
				&& arr[i-2] === '.' 
				&& validation(/[A-z]+/g, arr[i-1])
				&& arr[secondBracket + 1] === '*'
				&& validation(/\d+/, [secondBracket + 2])) {
                   for(let j = 0; j < arr[secondBracket+2]; j++){
				      section.insertAdjacentText('beforeend', `<${arr[i-3]} class='${arr[i-1]}'>${text[0].join('')}</${arr[i-3]}>`);
				      section.insertAdjacentHTML("beforeend", '</br>')
	                 } 
	        }

			else if (arr[i-3] 
				&& validation(/[A-z]+/g, arr[i-3]) 
				&& arr[i-2] === '#' 
				&& validation(/[A-z]+/g, arr[i-1])
				&& arr[secondBracket + 1] === '*'
				&& validation(/\d+/, [secondBracket + 2])) {
                   for(let j = 0; j < arr[secondBracket+2]; j++){
				      section.insertAdjacentText('beforeend', `<${arr[i-3]} id='${arr[i-1]}'>${text[0].join('')}</${arr[i-3]}>`);
				      section.insertAdjacentHTML("beforeend", '</br>')
	                 } 
	        }
	}
		       // 		*** КІНЕЦЬ БЛОКУ З ТЕКСТОМ ***

		//		якщо є тег і є клас
		if (validation(/[a-z]+/, el)
			&& arr[i + 1] === '.'
			&& arr[i + 2]
			&& validation(/[A-z]+|-|_/, arr[i + 2])
			&& validation(/[a-z]+/, el) 
			&& (arr[i - 1] !== '>' || !arr[i-1]) 
			&& arr[i + 3] !== '>'
			&& arr[i + 3] !== '*'
			&& arr[i + 3] !== '{'
			) {
			section.insertAdjacentText('beforeend', `<${el} class ="${arr[i + 2]}"></${el}>`);
			section.insertAdjacentHTML('beforeend', `</br>`);

			// 		якщо є тег і є id
		} else if (validation(/[a-z]+/, el)
		&& arr[i + 1] === '#'
		&& arr[i + 2]
		&& validation(/[A-z]+|-|_/, arr[i + 2])
		&& validation(/[a-z]+/, el) 
		&& (arr[i - 1] !== '>' || !arr[i-1]) 
		&& arr[i + 3] !== '>'
		&& arr[i + 3] !== '*'
		&& arr[i + 3] !== '{'
		) {
		section.insertAdjacentText('beforeend', `<${el} id ="${arr[i + 2]}"></${el}>`);
		section.insertAdjacentHTML('beforeend', `</br>`);


			//     *** БЛОК МНОЖЕННЯ ***

			//      якщо множення тегу
		} else if (el === '*' && validation(/[a-z]+/, arr[i - 1])
			&& validation(/\d+/, arr[i + 1])
			&& (validation(/[a-z]+/, arr[i-1]) && arr[i - 2] !== '>')
			&& arr[i - 2] !== '.'
			&& arr[i-2] !== '#') {
			for (let j = 0; j < arr[i + 1]; j++) {
				section.insertAdjacentText('beforeend', `<${arr[i-1]}></${arr[i-1]}>`);
				section.insertAdjacentHTML('beforeend', `</br>`);
			}		
			// 		якщо множення з тегу з класом
		} else if (el === '*' && validation(/[a-z]+/, arr[i - 1])
			&& validation(/\d+/, arr[i + 1])
			&& arr[i - 2] === '.'
			&& arr[i - 3]
			&& (validation(/[a-z]+/, arr[i-3]) && arr[i - 4] !== '>')
			&& validation(/[a-z]+/, arr[i - 3])) {
			for (let j = 0; j < arr[i + 1]; j++) {
				section.insertAdjacentText('beforeend', `<${arr[i - 3]} class = "${arr[i - 1]}"></${arr[i - 3]}>`);
				section.insertAdjacentHTML('beforeend', `</br>`);
			}
			// 		якщо множення тегу з id
		} else if (el === '*' && validation(/[a-z]+/, arr[i - 1])
		&& validation(/\d+/, arr[i + 1])
		&& arr[i - 2] === '#'
		&& arr[i - 3]
		&& (validation(/[a-z]+/, arr[i-3]) && arr[i - 4] !== '>')
		&& validation(/[a-z]+/, arr[i - 3])) {
		for (let j = 0; j < arr[i + 1]; j++) {
			section.insertAdjacentText('beforeend', `<${arr[i - 3]} id = "${arr[i - 1]}"></${arr[i - 3]}>`);
			section.insertAdjacentHTML('beforeend', `</br>`);
		}
		}
		    //  	*** КІНЕЦЬ БЛОКУ МНОЖЕННЯ ***

			// 		якщо просто тег
		else if (((validation(/[a-z]+/, el) && !arr[i - 1])
		 || (validation(/[a-z]+/, el)
		&& arr[i - 1] !== '.'
		&&  arr[i - 1] !== '#'))
		&&  arr[i + 1] !== '.'
		&&  arr[i + 1] !== '>'
		&&  arr[i + 1] !== '*'
		&&  arr[i + 1] !== '{'
		&&  arr[i + 1] !== '#') {
		section.insertAdjacentText('beforeend', `<${el}></${el}>`);
		section.insertAdjacentHTML('beforeend', `</br>`);
	}    
	})
}

export const nestingWithAttribute = (arr, sign = '.', attr = 'class') => {
	
	arr.forEach((el, i) => {
		// 		вкладеність з тегом і класом або id
		if (el === '>'
		 && validation(/[a-z]+/,arr[i - 1])
		 && arr[i - 2] === sign 
		 && validation(/[a-z]+/,arr[i - 3])) {
			let slicedArr = arr.slice(i + 1);
			let nestedSignArr = [];
			slicedArr.forEach((el, i) => {
				if (el === '>' || el === '^') {
					nestedSignArr.push(i)
				}
			})
			let nestedArr = slicedArr.slice(0, nestedSignArr[0]);
			section.insertAdjacentText('beforeend', `<${arr[i - 3]} ${attr}="${arr[i - 1]}">`)
			section.insertAdjacentHTML('beforeend', `</br>`)
				    linearCodeGen(nestedArr);
			section.insertAdjacentText('beforeend', `</${arr[i - 3]}>`);
		}

	})
}
// 		вкладеність без тегу але з класом або id
export const nestingWithoutTag = (arr, sign = '.', attr = 'id') =>{
	
	arr.forEach((el, i) =>{
	if(validation(/[a-z]+/, el) 
	    && arr[i-1] === '>'
		&& arr[i-3] === sign
		&& (!arr[i-4] || !validation(/[A-z]+/g, arr[i-4]))){
			let slicedArr = arr.slice(i);
			let nestedSignArr = [];
			slicedArr.forEach((el, i) => {
				if (el === '>' || el === '^') {
					nestedSignArr.push(i)
				}
			})
			let nestedArr = slicedArr.slice(0, nestedSignArr[0]);
			section.insertAdjacentText('beforeend', `<div ${attr}="${arr[i - 2]}">`)
			section.insertAdjacentHTML('beforeend', `</br>`)
					linearCodeGen(nestedArr);
			section.insertAdjacentText('beforeend', `</div>`);	
		}
})
}
export const nestingWithoutAttribute = (arr) => {
	arr.forEach((el, i) => {
		// 		вкладеність без атрибутів
		if (validation(/[a-z]+/, el) 
		   && arr[i-1] === '>'
		   && arr[i - 3] !== '#' 
		   && arr[i - 3] !== '.') {
			let slicedArr = arr.slice(i);
			let nestedSignArr = [];
			slicedArr.forEach((el, i) => {
				if (el === '>' || el === '^') {
					nestedSignArr.push(i)
				}
			})
			let nestedArr = slicedArr.slice(0, nestedSignArr[0]);
			section.insertAdjacentText('beforeend', `<${arr[i - 2]}>`);
			section.insertAdjacentHTML('beforeend', `</br>`);
					linearCodeGen(nestedArr);
			section.insertAdjacentText('beforeend', `</${arr[i - 2]}>`);
		}
	})
}
