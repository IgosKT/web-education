/* Создай класс, который будет создавать пользователей с именем и фамилией. Добавить к классу метод для вывода имени и
фамилии */

class Person {
    type = "human";

    constructor (fName,lName) {
        this.name = fName;
        this.surname = lName;
    }

    print () {
        return `Ім'я: ${this.name} Прізвище:${this.surname} `
    }
}

let Igor = new Person("Ігор","Коцюба")
alert(Igor.print())
let Alex = new Person("Алекс","Пінер")
console.dir(Alex.print())

/* Создай список состоящий из 4 листов. Используя джс обратись к 2 li и с использованием навигации по DOM дай 1 элементу
синий фон, а 3 красный */

const [...list] = document.querySelectorAll('ul li')
console.log(list[1])
list.forEach((item,index)=>{
    if (index>0) item.style.backgroundColor = "yellow"
    if (index===0) item.style.backgroundColor = "blue"
})

/* Создай див высотой 400 пикселей и добавь на него событие наведения мышки. При наведении мышки выведите текстом
координаты, где находится курсор мышки */

const div = document.querySelector(".task-3") 

div.addEventListener("mousemove", (e) => {
    div.textContent = `X: ${e.clientX}px , Y: ${e.clientY}px`
})

/* Создай кнопки 1,2,3,4 и при нажатии на кнопку выведи информацию о том какая кнопка была нажата */

const [...inputs] = document.querySelectorAll('label input'),
[...label] = document.querySelectorAll('label');

inputs.forEach((item,index)=>{
    item.addEventListener("click",(e)=>{
       //label[index].prepend(`${item.value}`)
       label[index].insertAdjacentHTML("afterbegin",`${item.value}`)
    })
})

/* Создай див и сделай так, чтобы при наведении на див, див изменял свое положение на странице */

const div5 = document.querySelector(".task-5")
let step = 600

div5.addEventListener("mouseover",(e)=>{
    if(div5.offsetLeft >= document.documentElement.offsetWidth-step || div5.offsetLeft+step < 0 ) { step *= -1}
    div5.style.left =  div5.offsetLeft + step+'px' //ПИТАННЯ:ЧИ МОЖНА ЯКОСЬ ЗМІНИТИ ПОЗИЦІОНУВАННЯ ОДРАЗУ ЧЕРЕЗ .offset?
})

/* Создай поле для ввода цвета, когда пользователь выберет какой-то цвет сделай его фоном body */

const input6 = document.querySelector("#color")

input6.onchange = ()=>{
    document.body.bgColor = `${input6.value}`
    //document.documentElement.style.backgroundColor = `${input6.value}`
    //ПИТАННЯ:  ЯКА РІЗНИЦЯ  МІЖ document.body І document.documentElement?
}

/* Создай инпут для ввода логина, когда пользователь начнет Вводить данные в инпут выводи их в консоль */

const input7 = document.querySelector("#input7")
input7.addEventListener('input', ()=>{
    console.log(input7.value)
})

/* Создайте поле для ввода данных поcле введения данных выведите текст под полем */

const div8 = document.querySelector(".task-8")
div8.firstElementChild.addEventListener('input', ()=>{
    div8.lastElementChild.textContent = `${div8.firstElementChild.value}`
    //input8.insertAdjacentHTML("afterend",`<span>${input8.value}</span>`)
})

