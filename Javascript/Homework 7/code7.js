/* При загрузке страницы - показать на ней кнопку с текстом "Нарисовать круг". Данная кнопка должна являться единственным контентом 
в теле HTML документа, весь остальной контент должен быть создан и добавлен на страницу с помощью Javascript
При нажатии на кнопку "Нарисовать круг" показывать одно поле ввода - диаметр круга. При нажатии на кнопку "Нарисовать" создать на
странице 100 кругов (10*10) случайного цвета. При клике на конкретный круг - этот круг должен исчезать, при этом пустое место
заполняться, то есть все остальные круги сдвигаются влево. */

const btn = document.querySelector("input");

btn.onclick = () => {
  const input1 = document.createElement("input"),
  drawBtn = document.createElement("input");
 // let d = request();
  btn.hidden = true;
  input1.type = ("number");
  input1.placeholder = ("Введіть діаметр > 10");
  input1.min = (10);
  drawBtn.type = ("button");
  drawBtn.value = ("Створити");

  document.body.prepend(drawBtn);
  document.body.prepend(input1);

  //--------створення кругів-----------------------
  drawBtn.onclick = () => {
    let d = input1.value;
    check(d);

    for (let i = 1; i <= 100; i++) {
      const brk = document.createElement("br"),
      circle = document.createElement("div");

      circle.className = "class-circle";
      circle.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, 50%, 50%)`;
      circle.style.height = `${d}px`;
      circle.style.width = d + "px";
      circle.onclick = () => circle.remove();

      document.body.prepend(circle);
      if (i % 10 === 0) {document.body.prepend(brk)};
    }
  }
};
//------------перевірка---------------------------
function check(diam) {
  if (diam < 10 || isNaN(diam)) {
    while (diam < 10 || isNaN(diam)) {
      alert('Введіть значення "d" > 10');
      return diam.remove();
    }
    return diam;
  }
}
  //------------варіант 2---------------------------
  /*   for(let i =0; i<10;i++) {
    const brk = document.createElement("br");
      for(let j = 0; j<10;j++) {
      let circle = document.createElement("div");
      circle.className = "class-circle";
      circle.style.backgroundColor = `hsl(${Math.floor(Math.random()*360)}, 50%, 50%)`;
      circle.style.height = `${d}px`;
      circle.style.width = d+"px";
      circle.onclick = ()=> circle.remove();
      document.body.prepend(circle);
      }
      document.body.prepend(brk);
  } */



/* function request() {
  let diam = parseFloat(prompt('Введіть діаметр круга "d"', '"d" > 0'));
  if (diam <= 0 || isNaN(diam)) {
    while (diam <= 0 || isNaN(diam)) {
      diam = parseFloat(prompt('Введіть валідне значення "d"', '"d" > 0'));
    }
  }
  return diam;
} */
