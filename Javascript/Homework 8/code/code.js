/*Створіть на сторінці div і дайте йому зовнішній відступ 150 пікселів. Використовуючи JS виведіть у консоль відступ
Створіть програму секундомір.
* Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"
* При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий *
 Виведення лічильників у форматі ЧЧ:ММ:СС
* Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції
*/


/*const div = document.createElement("div");
document.body.prepend(div); 
console.log(getComputedStyle(div).margin);*/


const btnStart = document.getElementById("start"), 
btnStop = document.getElementById("stop"), 
btnReset = document.getElementById("reset");

const stopwatch = document.querySelector(".container-stopwatch"),
sec = document.getElementById("sec"),
min = document.getElementById("min"),
hour = document.getElementById("hour");

let seconds = 0,
minutes = 0,
hours = 0,
counter;
//let isClick = false;

btnStart.onclick = () =>{
    clearClass();
    stopwatch.classList.add("green"); 
    clearInterval(counter);
    //if (isClick===false){
    counter = setInterval( ()=> {
        seconds++;
        if(seconds === 60){
            seconds=0;
            minutes ++;
        };
        if(minutes === 60){
            minutes=0;
            hours ++;
        };
        show();
    }, 1000);
    //}
    //isClick = true;
};

btnStop.onclick = () =>{
    clearClass();
    stopwatch.classList.add("red");
    clearInterval(counter);
};

btnReset.onclick = () =>{
    clearClass();
    stopwatch.classList.add("silver");
    seconds=0;
    minutes=0;
    hours=0;
    show ();
}

function clearClass () {
    stopwatch.classList.remove("red");
    stopwatch.classList.remove("black");
    stopwatch.classList.remove("green");
    stopwatch.classList.remove("silver");
}

function show () {
    if(seconds<10) {
        sec.innerText = "0" + seconds
    } else {sec.innerText = seconds};

    if(minutes<10) {
        min.innerText = "0" + minutes
    } else {min.innerText = minutes;};

    if(hours<10) {
        hour.innerText = "0" + hours
    } else {hour.innerText = hours};   
}

/* Реалізуйте програму перевірки телефону
* Використовуючи JS Створіть поле для введення телефону та кнопку збереження
* Користувач повинен ввести номер телефону у форматі 000-000-00-00
* Після того як користувач натискає кнопку зберегти перевірте правильність введення номера, якщо номер правильний зробіть зелене тло і використовуючи document.location 
переведіть користувача на сторінку https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg якщо буде помилка, відобразіть її в діві до input. */

const div = document.createElement('div'),
input = document.createElement('input'),
btn = document.createElement('input');

input.type = ("tel");
input.placeholder = ("000-000-00-00");
btn.type = ("button");
btn.value = ("Зберегти");
div.className = ("Phone")

stopwatch.after(div);
div.append(input);
div.append(btn);

btn.onclick = () => {
    let number = input.value;
    let pattern = /\d\d\d-\d{3}-\d{2}-\d{2}$/;
    if(pattern.test(number)) {
        console.log(number);
        document.body.style.backgroundColor = 'green';
        document.location.href = 'https://bbtr.siberianhealth.com/upload/iblock/caa/266_1273.jpg?1596188710316688';
    }else{
        btn.after("Введіть номер у форматі 000-000-00-00")
    }
}

/* Створіть слайдер кожні 3 сек змінюватиме зображення
Зображення для відображення */

const div2 = document.createElement('div');

let img1='https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg',
img2='https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg',
img3='https://naukatv.ru/upload/files/shutterstock_418733752.jpg',
img4='https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg',
img5='https://storage.googleapis.com/pod_public/1300/120927.jpg',
i=0;

let arr = [img1,img2,img3,img4,img5];

div2.className = ("slider");
div.after(div2);

setInterval(()=>{
    i++;
    div2.style.backgroundImage = `url(${arr[i % arr.length]})`;
},3000);


