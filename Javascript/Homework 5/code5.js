//------------------Задача 1---------------------
let obj = {};

function isEmpty(obj) {
  return Object.entries(obj).length === 0 ? true : false;
}

/* obj["key"]= "value"; */

alert(isEmpty(obj));

//------------------Задача 2-3---------------------

function Human(name, age, birth) {
  (this.name = name),
  (this.age = age),
  (this.birth = birth),
  Human.counter++;
}

Human.genus = "Homosapiens";
Human.counter = 0;

/* const serj = new Human("Serg",39, 1983);
const daron = new Human("Daron",38, 1984);
const shavo = new Human("Shavo",40, 1982);
const john = new Human("John",37, 1985); */

const systemOfaDown = new Array(
  new Human("Serj", 39, 1983),
  new Human("Daron", 38, 1984),
  new Human("Shavo", 40, 1982),
  new Human("John", 37, 1985)
);
//----------Сортування---------------------------
function sortAge(value) {
  systemOfaDown.sort(function (a, b) {
    return a[value] - b[value];
  });
}

sortAge("age");
//-------------------Вивід данних----------------
systemOfaDown.forEach(function (value) {
  document.write(`${value.name};`);
});
/* document.write("<br> Осіб виду "+Human.genus+" створено "+Human.counter+" шт."); */
document.write(`<br> Осіб виду "${Human.genus}" створено ${Human.counter} шт.`);

/* class User {
    constructor(name,age) {
      this.name = name;
      this.age = age;
    }

    gender = "man";

    sayHi() {
      alert(this.name);
    }
  }

  // Использование:
  let Ivan = new User('Иван',32);
  Ivan.sayHi();

  let Igor = new User('Igor',28);
  Igor.sayHi(); */

/* class Animal {
    constructor(name) {
      this.speed = 0;
      this.name = name;
    }
  }
  
  class Rabbit extends Animal {
    constructor(earLength) {
      super(); // без супер не працюватиме
      this.earLength = earLength;
    }
  }
  
  // теперь работает
  let rabbit = new Rabbit("Белый кролик", 10);
  alert(rabbit.name); // Белый кролик
  alert(rabbit.earLength); // 10

 */
