/* * У папці calculator дана верстка макета калькулятора. Необхідно зробити цей 
калькулятор робітником.<br>
* При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на 
табло калькулятора.<br>
* При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не 
відбувається - програма чекає введення другого числа для виконання операції.<br>
* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, 
так і будь-якого з операторів, в табло повинен з'явитися результат
виконання попереднього виразу.<br>
* При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає,
що в пам'яті зберігається число. Натискання на MRC 
покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять. */



window.addEventListener("DOMContentLoaded",()=>{
    //---------------------отримання ДОМ----------------------------------------
    const keys = document.querySelector(".keys"),
    display = document.querySelector(".display > input"),
    clearBtn = document.querySelector(".clear"),
    rezBtn = document.querySelector(".orange"),
    memSign = document.createElement("span");
    memSign.innerText = "m";

    const numbers = [...document.querySelectorAll(".black")].map((item)=>{
        //if(item.value !== "C")
            return item.value
    }),
    clearAllOperator = numbers.pop(),
    operators = [...document.querySelectorAll(".pink")].map((item)=>{
        return item.value
    }),
    memArr = [...document.querySelectorAll(".gray")].map((item)=>{
        return item.value
    });
    
    console.dir(numbers)
    console.dir(operators)
    console.dir(memArr)

    const calc = {
        value1 : "",
        value2 : "",
        sign: "",
        flag: false,
        memory: "",
        memCount: 0,
        calculate: function() {
            switch(this.sign) {
                case "+" : this.value1 = parseFloat(this.value1) + (+this.value2)
                break;
                case "-" : this.value1 = this.value1 - this.value2
                break;
                case "*" : this.value1 = this.value1 * this.value2
                break;
                case "/" : if(this.value2 === '0') {
                    alert("Error")
                    this.value2 = "";
                }else {
                    this.value1 = this.value1/this.value2
                }
                break;
            }
            this.flag = true; 
            show(this.value1, display);
        },
        clearAll:  function() {
            this.value1 = "";
            this.value2 = "";
            this.sign = "";
            this.flag = false;
            display.value = "0";
            rezBtn.setAttribute('disabled',true);
        }
    };

    //---------------------------ПОДІЇЇ-------------------------------------------
    
    keys.addEventListener("click", function (e) {
        let key = e.target.value;

        if(numbers.includes(key)) { 
            if(calc.value2 ==="" && calc.sign === ""){
                calc.value1 += key;
                show(calc.value1, display);
            } else if(calc.value1 !== "" && calc.value2 !== "" && calc.flag){
                calc.value2 = key;
                calc.flag = false;
                show(calc.value2, display);                
            } else {
                calc.value2 += key;
                show(calc.value2, display);
            }
        };

        if(operators.includes(key)) {
            rezBtn.removeAttribute('disabled');

            if(calc.value1 !=="" && calc.sign === "") {
                calc.sign = key; 
                return;
             }else if(calc.value2 !=="" && calc.sign !== "" && !calc.flag) {
                calc.calculate();
                calc.sign = key;
             }
            return
        };

        if(memArr.includes(key)) {
            if(key === "m+") {
                document.querySelector(".display").prepend(memSign);
                calc.memCount = 0;
                calc.memory = calc.value1;
            }else if(key === "m-") {
                calc.memCount = 0;
                calc.memory = "";
                memSign.remove();
            }else if(key === "mrc") {
                if(calc.memCount === 0 && calc.memory !== ""){ //if(calc.memory == "")
                    calc.memCount++;
                    if(calc.value1 =="" && calc.value2 == "") {calc.value1 = calc.memory}
                    else {calc.value2 = calc.memory};
                    show(calc.memory, display); 
                }else if(calc.memCount === 1) { //if(calc.memory !== "")
                    calc.memory = "";
                    calc.memCount = 0;
                    memSign.remove();
                }
                console.log(calc)
                return    
            };
            console.log(calc)
            return
        };

        if(e.target == rezBtn) {
            calc.calculate();
            calc.sign = "";
        };

        if(e.target.type !== "button") return;
        if(e.target == clearBtn) calc.clearAll();

        console.log(calc) 
    });
    
    function show (data, el) {
    el.value = data
    };
});

