/* Створіть сайт конструктор піци 
Потрібно зробити так, щоб при виборі розміру піци ціна у залежності від коржа 
змінювалась.
Також користувач має мати змогу додавати топінги та соуси у необмеженій кількості
на корж, топінги та соуси повинні показуватись на коржі та додатись до списку інгредієнтів 
та враховуватись у вартість піци.
Поле форми з даними користувача  перевірити на правильність вводу,кнопка отримати знижку
 має втікати від користувача */

window.addEventListener("DOMContentLoaded",()=>{
//-----------------------------------------------------------------------------------
    const [...ingridients] = document.querySelectorAll(".draggable"),
    [...size] = document.querySelectorAll(".radioIn"),
    target = document.querySelector(".table"),
    bortik = document.querySelector(".table>img"),
    price = document.querySelector(".price"),
    sauces = document.querySelector(".sauces"),
    topings = document.querySelector(".topings"),
    cancel = document.getElementsByName('cancel')[0],
    submit = document.getElementsByName('btnSubmit')[0],
    name = document.getElementsByName('name')[0],
    phone = document.getElementsByName('phone')[0],
    email = document.getElementsByName('email')[0],
    banner = document.querySelector("#banner");
    
    let bortikCost = 0,
    ingrCost = 0,
    totalCost = 0,
    removeElementCost = 0,
    imgToRemove = 0,
    removeCosts = [],
    usedIngridients=[],
    [...removingImg] = target.children;
// соуси по 5грн,сири по 7грн,добавки по 10грн
    const ingridientsCosts=[
        {sauceClassic:5},
        {sauceBBQ:5},
        {sauceRikotta:5},
        {moc1:7},
        {moc2:7},
        {moc3:7},
        {telya:10},
        {vetch1:10},
        {vetch2:10}
    ],
    bortikSize = [{small:200},{mid:255},{big:315}];
    

    function sum (arr){
        //debugger
        let rez=null;
        arr.forEach((value)=>{
            rez = rez+value       
        });
        return rez;
    }
    
//--------------------------------------------------------------------------------    
    ingridients.forEach((value)=>{
        value.addEventListener("dragstart", function (evt) {
            evt.dataTransfer.effectAllowed = "move";
            evt.dataTransfer.setData("Text", this.id);
        }); 
    });

    target.addEventListener("dragover", function(evt) {
        if (evt.preventDefault) evt.preventDefault();
        return false;
    });

    target.addEventListener("dragenter", function(evt) {
        this.firstElementChild.style.width = "104%"
    });

    target.addEventListener("dragleave", function(evt) {
        this.firstElementChild.style.width = "100%"
    });

    target.addEventListener("drop", function(evt) {
        if (evt.preventDefault) evt.preventDefault();
        if (evt.stopPropagation) evt.stopPropagation();
        this.firstElementChild.style.width = "100%";

        let getId = evt.dataTransfer.getData("Text"),
        getElem = document.getElementById(getId);
        ingrImg = document.createElement("img");
        
//---------------------перетаскування інгредієнтів---------
        ingridients.forEach((value,index)=>{
            if(getId === value.id) {
                ingrImg.src = value.src;
                ingrCost = Object.values(ingridientsCosts[index])[0];
                usedIngridients.push(ingrCost);

                console.log(ingrCost);
                console.log(usedIngridients);
            }
        });

        this.append(ingrImg);
//----------------------------формування цін та спиcку----------------------
        if(totalCost===0) {
           size.forEach((value)=>{
            if(value.id==='small' && value.checked) bortikCost = bortikSize[0].small;
            if(value.id==='mid' && value.checked) bortikCost = bortikSize[1].mid;
            if(value.id==='big' && value.checked) bortikCost = bortikSize[2].big;
            });

            price.insertAdjacentHTML('beforeend',
                `<p>${totalCost=bortikCost+ingrCost}грн</p>`
            );
        }else {    
            totalCost = totalCost+ingrCost;       
            price.lastElementChild.textContent = totalCost+'грн';
        };

        if(getId === ingridients[0].id || getId === ingridients[1].id || getId === ingridients[2].id){
            sauces.lastElementChild.insertAdjacentHTML('beforeend', 
                `<span>${getElem.nextElementSibling.textContent}</span>
                <br>`
            );
        }else{
            topings.lastElementChild.insertAdjacentHTML('beforeend', 
                `<span>${getElem.nextElementSibling.textContent}</span>
                <br>`
            );
        };
    });

    size.forEach((value)=>{
        value.addEventListener("click", function(evt){
            //debugger
            if(totalCost>0 && evt.target.id==="small"){
                totalCost = (bortikSize[0].small + sum(usedIngridients)) - sum(removeCosts);
                price.lastElementChild.textContent = totalCost+'грн';
            }else if(totalCost>0 && evt.target.id==="mid"){
                totalCost = (bortikSize[1].mid + sum(usedIngridients)) - sum(removeCosts);
                price.lastElementChild.textContent = totalCost+'грн';
            }else if(totalCost>0 && evt.target.id==="big"){
                totalCost = (bortikSize[2].big + sum(usedIngridients)) - sum(removeCosts);
                price.lastElementChild.textContent = totalCost+'грн';
            }return
        });
    });
//---------------------видалення інгрідієнтів-----------------
sauces.addEventListener("click", function(evt){
    if(evt.target.textContent==='Соуси:' || evt.target.className==='sauces') return;
    evt.target.nextElementSibling.remove();
    evt.target.remove();
    
    ingridients.forEach((elem,index)=>{
        if(elem.nextElementSibling.textContent===evt.target.textContent){
            removeElementCost = ingridientsCosts[index][elem.id];
            totalCost = totalCost - removeElementCost;
            price.lastElementChild.textContent = totalCost+'грн';
            removeCosts.push(removeElementCost);
          
            imgToRemove = elem.src;
        }
    }); 

    [...removingImg] = target.children;
    removingImg.forEach((elem)=>{
        if(elem.src === imgToRemove){elem.remove()}
    });
});

sauces.addEventListener("mouseover", function(evt){
    if(evt.target.textContent==='Соуси:' || evt.target.className==='sauces') return;
    evt.target.style.cursor = ("pointer"); 
    evt.target.style.textDecoration = ("line-through");
});

sauces.addEventListener("mouseout", function(evt){
    evt.target.style.cursor = "";
    evt.target.style.textDecoration = "";
});

topings.addEventListener("click", function(evt){
    if(evt.target.textContent==='Топiнги:' || evt.target.className==='topings') return;
    evt.target.nextElementSibling.remove();
    evt.target.remove();

    ingridients.forEach((elem,index)=>{
        if(elem.nextElementSibling.textContent===evt.target.textContent){
            removeElementCost = ingridientsCosts[index][elem.id];
            totalCost = totalCost - removeElementCost;
            price.lastElementChild.textContent = totalCost+'грн';
            removeCosts.push(removeElementCost);

            imgToRemove = elem.src;
        }
    });
    
    [...removingImg] = target.children;
    removingImg.forEach((elem)=>{
        if(elem.src === imgToRemove){elem.remove()}
    });
});

topings.addEventListener("mouseover", function(evt){
    if(evt.target.textContent==='Топiнги:' || evt.target.className==='topings') return;
    evt.target.style.cursor = ("pointer");
    evt.target.style.textDecoration = ("line-through");
});

topings.addEventListener("mouseout", function(evt){
    evt.target.style.cursor = "";
    evt.target.style.textDecoration = "";
});

//---------------------валідація------------------------------    
    name.addEventListener("blur",()=>{
        if(!/^[a-zA-Zа-яА-ЯіІ'][a-zA-Zа-яА-Я-'іІ ]+[a-zA-Zа-яА-Я'іІ]?$/u.test(name.value)) {
            name.classList.add("error")
        }else{
            name.value = name.value.replace(name.value[0],name.value[0].toUpperCase())
        }
    });

    name.addEventListener("focus",()=>{
        name.classList.remove("error")
    });

    phone.addEventListener("blur",()=>{
        if(!/^\+380\d{9}$/.test(phone.value)) { //[\+]\d{3}\s[\(]\d{2}[\)]\s\d{3}[\-]\d{2}[\-]\d{2}
            phone.classList.add("error") 
        }
    });

    phone.addEventListener("focus",()=>{
        phone.classList.remove("error")
    });

    email.addEventListener("blur",()=>{
        if(!/^[\w-\.]+@[\w-]+\.[a-z]{2,4}$/i.test(email.value)) { 
            email.classList.add("error") 
        }
    });

    email.addEventListener("focus",()=>{
        email.classList.remove("error")
    });
//------------------------------кнопки--------------------------------------------------

    cancel.onclick = () => {
        totalCost = 0;
        usedIngridients=[];
        removeElementCost = 0;
        removeCosts=[],
        imgToRemove=0;
        if(price.lastElementChild.textContent==='Цiна:') return;
        price.lastElementChild.remove();
        sauces.lastElementChild.innerHTML="";
        topings.lastElementChild.innerHTML="";
        target.innerHTML='<img src="Pizza_pictures/klassicheskij-bortik_1556622914630.png" alt="Корж класичний">';
    }

    submit.addEventListener("click", (e)=> {
        if( name.className==='error' ||
            name.value ==="" ||
            phone.className==='error' ||
            phone.value ==="" ||
            email.className==='error' ||
            email.value ===""
            ) {
            e.preventDefault();
            alert('Введіть дані')
        }else if(price.lastElementChild.textContent==='Цiна:') {
            e.preventDefault();
            alert('Оберіть піцу')
        }else if(price.lastElementChild.textContent===bortikSize[0].small+"грн" ||
            price.lastElementChild.textContent===bortikSize[1].mid+"грн"  ||
            price.lastElementChild.textContent===bortikSize[2].big +"грн" 
            ) {
            e.preventDefault();
            alert('Оберіть соуси')
        }else{
        document.location.href = "./thank-you.html"}
    });

//-------------------------------тікаюча кнопка--------------------------------------
   
    banner.addEventListener('mouseover',(e)=>{
        let bannerCoords = banner.getBoundingClientRect(),
        minStepX = e.clientX, 
        minStepY = e.clientY,
        screenWidth = document.documentElement.clientWidth - bannerCoords.width-50,
        screenHeight = document.documentElement.clientHeight - bannerCoords.height-50;

        banner.style.right = Math.floor(Math.random()*(screenWidth-minStepX)+minStepX)+'px';
        banner.style.bottom = Math.floor(Math.random()*(screenHeight-minStepY)+minStepY)+'px';
        banner.style.transition = "0.2s";
    })

},false)
