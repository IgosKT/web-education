/* Створіть сайт з коментарями. Коментарі тут : https://jsonplaceholder.typicode.com/
    Сайт має виглядати так : https://kondrashov.online/images/screens/120.png
    На сторінку виводити по 10 коментарів, у низу сторінки зробити поле пагінації 
    (перемикання сторінок) при перемиканні сторінок показувати нові коментарі. 
    з коментарів виводити : 
    "id": 1,
    "name"
    "email"
    "body": */

    

    /* function getData(callback) {
      const xhr = new XMLHttpRequest();
      xhr.open("get", "https://jsonplaceholder.typicode.com/comments");
      xhr.onreadystatechange = function () {
        if ((xhr.readyState === 4) & (xhr.status === 200)) {
          let data = JSON.parse(xhr.response);
          callback(data);
        }
      };
      xhr.send();
    }

    function main(data){
       console.log(data); 
    }
    
    getData(main) */
    //import fetch from "node-fetch";


    //-----------------ЗАПИТ-------------------------------------------
    async function getData(url) {
      const response = await fetch(url); // === .then((responce)=>{return responce})
      const data = await response.json(); // === const data = response.json().then((responce2) => {console.log(responce2);});
      
      return data;//
      
      // ------ТЕ САМЕ-------------------------------
      //const request = fetch(url);
      //.then((responce)=>{return responce.json()})
      //.then((responce2)=>{console.log(responce2)})
    }

    async function main (){
      const commentsArr = await getData("https://jsonplaceholder.typicode.com/comments");
      
      let currentPage = 1;
      let commentsNumbers = 10;
      console.log(currentPage)//?
      

      //------------------------СТВОРЕННЯ КОМЕНТАРІВ-----------------------------
      function displayComments(commentsArr, currentPage, commentsNumbers) {
        const comments = document.querySelector(".comments"); 
        
        comments.innerHTML = "";
        
        const start = (currentPage - 1) * commentsNumbers; //?
        const end = start + Number(commentsNumbers) ;
        const paginatedCommentsArr = commentsArr.slice(start, end); //ВИБІР КОМЕНТАРІВ ДЛЯ ВІДТВОРЕННЯ

        paginatedCommentsArr.forEach(({ id, name, email, body }) => {
          const commentCard = `
                <div class="comment-card">
                    <div><b>Comment№</b> <b>${id}</b></div>
                    <div>
                        <span><b>name:</b> ${name}</span>
                        <span><b>email:</b> ${email}</span>
                    </div>
                    <div><b>Comment:</b> ${body}</div>
                </div>`;
          comments.insertAdjacentHTML("beforeend", commentCard);
        });
      }
      //-------------СТВОРЕННЯ ПАГІНАЦІЇ------------------------------------------
      function displayPagination(commentsArr, commentsNumbers, currentPage) {
        const paginationUL = document.querySelector(".pagination__list");
        const paginationCount = Math.ceil(commentsArr.length / commentsNumbers); //К-ТЬ КНОПОК ПАГІНАЦІЇ

        paginationUL.innerHTML="";

        for (let i = 0; i < paginationCount; i++) {
          const paginationElement = `
            <li class="pagination__item">${i + 1}</li>`;
          paginationUL.insertAdjacentHTML("beforeend", paginationElement);
        }

        document.querySelectorAll(".pagination__item").forEach((el, ind) => {
          if (currentPage - 1 == ind) {
            el.classList.add("pagination__item--active");
          }
        });
        paginationUL.addEventListener("click", switchPagination);
      }
      //------------------ПЕРЕМИКАННЯ СТОРІНОК--------------------------------
      function switchPagination(e) {
        if (e.target.className !== "pagination__item") return;
        currentPage = e.target.textContent;

        const currentLi = document.querySelector(".pagination__item--active");
        currentLi.classList.remove("pagination__item--active");
        e.target.classList.add("pagination__item--active");

        displayComments(commentsArr, currentPage, commentsNumbers);
      }

      displayComments(commentsArr, currentPage, commentsNumbers);
      displayPagination(commentsArr, commentsNumbers, currentPage);

       const input = document.querySelector("input");
       input.onblur = function () {
         commentsNumbers = input.value;
         displayComments(commentsArr, currentPage, commentsNumbers);
         displayPagination(commentsArr, commentsNumbers, currentPage);
       }; 
    }

    main()
  
    