/* Виконати запит на https://swapi.dev/api/people (Star Wars: A Visual Guide) отримати список героїв 
зіркових воєн.Вивести кожного героя окремою карткою із зазначенням картинки Імені, статевої приналежності, 
ріст, колір шкіри, рік народження та планету на якій народився. */

window.addEventListener("DOMContentLoaded",()=>{
    const container =  document.querySelector(".container");

    function getRequest(url, fn = () => {}) {
      document.querySelector(".loader-box").classList.add("show");
      const xhr = new XMLHttpRequest();
      xhr.open("get", url);
      xhr.send();
      xhr.onreadystatechange = function () {
        if ((xhr.readyState == 4) & (xhr.status == 200)) {
          fn(JSON.parse(xhr.response));
        } else if (xhr.readyState === 4) {
          throw new Error(
            `Помилка у запиті на сервер : ${xhr.status} / ${xhr.statusText}`
          );
        }
      };
    }

    function showData(data){
      data.results.forEach(({ name, gender, height, skin_color, birth_year, homeworld }, index) => {
          const html = `
                <div class="card">
                    <div class="photo"><img src=https://starwars-visualguide.com/assets/img/characters/${index + 1}.jpg alt=""></div>
                    <div>${name}</div>
                    <div>Дата народження: ${birth_year}</div>
                    <div>Стать: ${gender}</div>
                    <div>Зріст: ${height}</div>
                    <div>Колір шкіри: ${skin_color}</div>
                    <div class="${homeworld}">Планета:
                        <a href="">${getRequest(homeworld, showPlanet)}</a>
                    </div>
                    <div class="btn" id="${name}">Save info</div>
                </div>`;
          container.insertAdjacentHTML("beforeend", html);
          
          //getRequest(homeworld, showPlanet);
          //<div>Планета:<a href="${homeworld}">${homeworld}</a></div>
        });
        
      const buttonsArr = document.querySelectorAll(".btn");
//-------ЗБЕРЕЖЕННЯ ДАНИХ------------------------------------------
      buttonsArr.forEach((btn, ind) => {
				btn.addEventListener("click", () => {
					const objToSave = data.results.find((el) => {
						return el.name === btn.id; // return el.name.includes(btn.id);
					});
          sessionStorage.setItem(btn.id, JSON.stringify(objToSave));
				});
			});

      document.querySelector(".loader-box").classList.remove("show");  
 //---------ВІДОБРАЖЕННЯ ПЛАНЕТ-----------------------------------    
      function showPlanet(planet) {
				[...document.getElementsByClassName(planet.url)].forEach((el) => { // planet.url === homeworld який э класом  <div class="${homeworld}">
						[...el.children][0].innerHTML = planet.name;
						[...el.children][0].href = planet.url;
					}
				);
			}
    }

    getRequest("https://swapi.dev/api/people",showData);

    
  /*   fetch("https://swapi.dev/api/people")//?
    .then((asd)=>{return asd.json()})
    .then((asd=>console.log(asd))) */


 
  
})
    
    



    /* const xhr = new XMLHttpRequest();
    xhr.open("get", "https://swapi.dev/api/people");
    xhr.send();
    xhr.onreadystatechange= function() {
        if ((xhr.readyState == 4) & (xhr.status == 200)) {
        showData(JSON.parse(xhr.response).results);
        } else if (xhr.readyState === 4) {
        throw new Error(
            `Помилка у запиті на сервер : ${xhr.status} / ${xhr.statusText}`
        );
        }
    } */

    
