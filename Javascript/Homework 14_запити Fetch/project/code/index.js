/*
Зробити програму з навігаційним меню яке буде показувати один з варіантів. 

Курс валют НБУ з датою на який день, 

героїв зоряних війн, 

список справ з https://jsonplaceholder.typicode.com/ 
//виводити які з можливістю редагування при натискані 
*/

//import fetch from "node-fetch";

window.onload = function () {

const req = async (url) => {
  document.querySelector(".box_loader").classList.add("show");
  const data = await fetch(url);
  return await data.json();
}


const nav = document.querySelector(".nav").addEventListener("click", async (e) => {
        if (e.target.dataset.link === "nbu") {
            req("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json")
                .then((info) => {
                    show(info)
                })
        } else if (e.target.dataset.link === "star") {
            // вивести всі 60 планет з https://swapi.dev/api/planets/
                let planet = await req("https://swapi.dev/api/planets"); // об'єкт планет
                
                const planets = new Array();
                planets.push(...planet.results);//planet.results - масив

                while (planet.next) {
                  let planetNext = await req(planet.next); //planet.next - посилання наступного запиту
                  planets.push(...planetNext.results); //planet.results - масив наступного запиту
                  planet = planetNext;
                }

                show(planets);  
                
        } else if (e.target.dataset.link === "todo") {
            req("https://jsonplaceholder.typicode.com/todos")
            .then((info) => {
                show(info)
            })
        } else {

        }
    })

function show(data = []) {
    if (!Array.isArray(data)) return;

    const tbody = document.querySelector("tbody");
    tbody.innerHTML = ""
    const newArr = data.map(
      ({ txt, rate, exchangedate, title, completed, name, diameter, population }, i) => {
        return {
          id: i + 1,
          name: txt || title || name,
          info1: rate || completed || diameter,
          info2: exchangedate || "" || population
        };
      }
    );

    newArr.forEach(({ name, id, info1, info2 }) => {
        tbody.insertAdjacentHTML("beforeend", `
        <tr> 
        <td>${id}</td>
        <td>${name}</td>
        <td>${info1}</td>
        <td>${info2}</td>
        </tr>
        `)
    })

    document.querySelector(".box_loader").classList.remove("show")
}
}
