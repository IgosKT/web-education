import { formCreate,buttonCreate,formValidate } from "./methods.js"

({
    'plugins': ['jsdom-quokka-plugin'],
    'jsdom': {"file": "index.html"}
})

//-----------СТОРІНКА АВТОРИЗАЦІЇ------------------------------------------
try {
    document.querySelector(".login").insertAdjacentHTML("beforeend", `
        <section class="container">
        <div class="login-img">
            <img src="./img/login 1.png" alt="login-img">
        </div>
        </section>
        <section class="container">
        <form class="login-form">
            <div class="tittle">Login</div>
            <div class="login-inputs">

            </div>
            <div class="login-entrance">
                <p>Not a user? <a href="./registration/">Register now</a></p>
            </div>    
        </form>
        </section>
    `)  

    formCreate("Email","email-login","email",".login-inputs") //(назва,id,тип,куди вставити)
    formCreate("Password", "password-login", "password", ".login-inputs") 
    
    buttonCreate("button", "login-btn", "Login", ".login-entrance")//(назва,клас,тип,куди вставити)
    
    document.querySelector(".login-btn").addEventListener("click", () => {
        alert("Користувача не знайденю, пройдіть реєстрацію")
    })

} catch (error) {
    if (!document.location.pathname.includes("/registration/")) {
        console.error(error)
    }
}

//-----------СТОРІНКА РЕЄСТРАЦІЇ------------------------------------------ 

try {
    document.querySelector(".registration").insertAdjacentHTML("beforeend", `
        <section class="container">
        <div class="registration-img">
            <img src="../img/Figure.png" alt="reg-img">
        </div>
        </section>
        <section class="container">
        <form class="registration-form">
            <div class="tittle">Registration form</div>
            <div class="registration-inputs">

            </div>
            <div class="registration-btn"> 

            </div>    
        </form>
        </section>
    `)   

    formCreate("Name", "fname", "text", ".registration-inputs"); //(назва,клас,тип,куди вставити)
    formCreate("Date of Birth", "birth", "date", ".registration-inputs");
    formCreate("Father’s/Mother’s Name", "lname", "text", ".registration-inputs");
    formCreate("Email", "email-reg", "email", ".registration-inputs");
    formCreate("Mobile No.", "mobile", "tel", ".registration-inputs","+380");
    formCreate("Password (від 8 символів)", "password-reg", "password", ".registration-inputs","мін. одна велика літера та одна цифра");
    formCreate("Re-enter Password", "re-password-reg", "password", ".registration-inputs");
    formCreate("home Number", "home-num", "number", ".registration-inputs");

    buttonCreate("submit", "reg-btn", "submit", ".registration-btn")

//----------------------ВАЛІДАЦІЯ-----------------------------------------------
    document.querySelectorAll("form input").forEach((el) => {
        if (el.type === "text" || el.type === "email" || el.type === "tel" || el.type === "password") {
            el.addEventListener("change", formValidate)
        }    
    })

    document.querySelector(".registration-form").addEventListener("submit", (evnt) => {  // не спрацбовуэ подія SUBMIT, якщо буде CLICK то все ок
        let isValid = []

        const [...inputs] = document.querySelectorAll("form input");
        //повторна перевірка із контекстом якщо користувач щось змінить
        inputs.forEach((el) => { 
            if (el.type === "text" || el.type === "email" || el.type === "tel" || el.type === "password") {
                formValidate.call(el)
            } 
        }) 
        isValid = inputs.filter((el) => {
            if (el.type === "text" || el.type === "email" || el.type === "tel" || el.type === "password") {
                return el.classList=="error"
            } 
        })
        if (isValid.length > 0) {
            console.log(isValid)
            evnt.preventDefault()
        } else {
            document.querySelector(".registration-form").action = "/index.html"
            alert("Дані збережено")
        }
})

} catch (error) {
    if (document.location.pathname.includes("/registration/")) {
        console.error(error)
    }
}


