
export function formCreate(labelName, inputId, inputType, place,placeholder="") {
    let label = document.createElement("label"),
        input = document.createElement("input");
    
    label.textContent = labelName
    label.htmlFor = inputId
    
    input.id = inputId
    input.type = inputType
    input.placeholder = placeholder
    input.required = true

    document.querySelector(place).append(label)
    document.querySelector(place).append(input)
}

export function buttonCreate(type,clss,text,place) {
    let button = document.createElement("button");

    button.type = type
    button.classList.add(clss);
    button.textContent = text

    document.querySelector(place).append(button)
}

export function formValidate() {
    if (this.type === "text" && validate(/^[A-zА-яіІґєЄї-]+$/i, this.value)) {
        this.classList.remove("error")
    } else if (this.type === "email" && validate(/^[A-z_0-9]+@[A-z-]+\.[A-z]{1,4}\.?[A-z]*$/i, this.value)) {
        this.classList.remove("error")
    } else if (this.type === "tel" && validate(/^\+380\d{9}$/, this.value)) {
        this.classList.remove("error")
    } else if (this.type === "password" && this.id === "re-password-reg") { // перевірка повторного паролю
        document.getElementById("password-reg").value === this.value ? this.classList.remove("error") : this.classList.add("error")
    } else if (this.type === "password" && validate(/(?=[\S]*([\d]))(?=[\S]*[A-ZА-ЯґҐєЄїЇ])[\S]{8,}/, this.value)) { //перевірка мінімум одна велика літера,одна цифра  довжиною 8 символів
        this.classList.remove("error")
    } else {
        this.classList.add("error");
    }
}

function validate(r,value) {
    return r.test(value)
}

