
window.addEventListener("DOMContentLoaded",()=>{

    const start = document.querySelector("#start"),
    exit = document.querySelector("#exit"),
    resume = document.querySelector("#resume"),
    reset = document.querySelector("#reset"),
    menu = document.querySelector(".menu"),
    table = document.querySelector(".table"),
    ball = document.querySelector(".ball"),
    accelerator = document.querySelector("#acceler"),
    computer = document.querySelector("#comp"),
    player1Racket = document.querySelector(".player1-racket"),
    player2Racket = document.querySelector(".player2-racket"),
    player1Count = document.querySelector(".player1-count"),
    player2Count = document.querySelector(".player2-count"),
    names = document.querySelector(".names"),
    rez = document.querySelector(".results"),
    tBody = document.querySelector(".results table tbody");

    /* console.log(table); */
    direction = () => {return Math.round(Math.random())===0 ? -1 : 1}

    let ballX,ballY,
    player1Name,player2Name,
    go,
    xSpeed = 1.5 * direction(),
    ySpeed = Math.random()*direction(),
    racketSpeed = 25,
    speedUP = 1,
    party = 0,
    allPlayers = [];

    class Player {
        constructor(name,points) {
            this.name = name;
            this.points = points;
        }
    }

    //----------------------------------------------------------------
    
    function ballPosition() {
      ballX = table.clientWidth / 2 - ball.clientWidth / 2;
      ballY = table.clientHeight / 2 - ball.clientHeight / 2;

      ball.style.left = ballX + "px";
      ball.style.top = ballY + "px";
    }

    function racketPosition () {
        player1Racket.style.left = 20 + "px"
        player1Racket.style.top = table.clientHeight/2 - player1Racket.clientHeight/2 + "px";

        player2Racket.style.left = table.clientWidth - player1Racket.clientWidth - 20 + "px";
        player2Racket.style.top = table.clientHeight/2 - player2Racket.clientHeight/2 + "px";
    }

    function moveBall () {
        if (ballY < table.offsetTop || ballY > table.offsetHeight - ball.clientHeight) {ySpeed *= -1}
        if (ballX < table.offsetTop) loose(player1Name)
        if (ballX > table.offsetWidth) loose(player2Name)

        if (
            (ballX <= table.offsetTop + player1Racket.offsetWidth + ball.clientWidth/2) && (ballY > player1Racket.offsetTop && ballY < player1Racket.offsetTop + player1Racket.offsetHeight) ||
            (ballX >= table.offsetWidth - player2Racket.offsetWidth - ball.offsetWidth *2 ) && (ballY > player2Racket.offsetTop && ballY < player2Racket.offsetTop + player2Racket.offsetHeight)
        ) {
            xSpeed *= -1 * speedUP // прискорення після кожного удару для форсування довгих партій
            ySpeed += 0.5 // коригування траєкторії 
            ySpeed *= direction() // випадковий фактор
            if(ballY==0 || ballY== -0) ySpeed += 1
        }

        ballX += xSpeed
        ballY += ySpeed
        
        ball.style.left = ballX + "px";
        ball.style.top = ballY + "px"; 
    }

    function loose(player) {
        ballPosition();
        xSpeed<0 ? xSpeed = 1.5 : xSpeed = -1.5
        ySpeed = Math.random()*direction();
     
        if(player==player1Name) {
            player2Count.textContent = parseInt(player2Count.textContent) + 1
        }else if(player==player2Name) {
            player1Count.textContent = parseInt(player1Count.textContent) + 1
        }
    }

    function computerMove () {

    }

    //----------------------------Ігровий процес-------------------------------------------------

    start.onclick = function () {
        player1Name = document.querySelector("#name-1").value,
        player2Name = document.querySelector("#name-2").value;
        names.firstElementChild.textContent = player1Name;
        names.lastElementChild.textContent = player2Name;
        menu.style.display="none";
        //menu.style.visibility = "hidden";
        accelerator.checked ? speedUP = 1.2 : speedUP = 1;
        ballPosition();
        racketPosition();
        go = setInterval(moveBall,1);    
    }

    addEventListener ("keydown", (e)=>{
        if (e.code === "KeyW" && player1Racket.offsetTop >= table.offsetTop+15) {player1Racket.style.top = player1Racket.offsetTop - racketSpeed + 'px'}
        if (e.code === "KeyS" && player1Racket.offsetTop <= table.offsetHeight-player1Racket.offsetHeight-15) {player1Racket.style.top = player1Racket.offsetTop + racketSpeed + 'px'}

        if (e.code === "ArrowUp" && player2Racket.offsetTop >= table.offsetTop+15) {player2Racket.style.top = player2Racket.offsetTop - racketSpeed + 'px'}
        if (e.code === "ArrowDown" && player2Racket.offsetTop <= table.offsetHeight-player2Racket.offsetHeight-15) {player2Racket.style.top = player2Racket.offsetTop + racketSpeed + 'px'}
    })

    exit.onclick = function () {
        
        let player1 = new Player(player1Name,player1Count.textContent),
        player2 = new Player(player2Name,player2Count.textContent);
        
        tBody.innerHTML = ""

        clearInterval(go)
        rez.style.display = "flex"
        allPlayers.push(player1,player2)
        allPlayers.forEach((item,index)=>{
            if(index % 2 === 0) {
                party++
                tBody.insertAdjacentHTML("beforeend",`<tr><td rowspan="2">${party}</td><td>${item.name}</td><td>${item.points}</td></tr>`)
            }else {
                tBody.insertAdjacentHTML("beforeend",`<td>${item.name}</td><td>${item.points}</td></tr>`)
            }            
        })
    }

    resume.onclick = function () {
        party = 0
        menu.style.display="flex"
        rez.style.display = "none"

        player1Count.textContent = 0
        player2Count.textContent = 0
    }

    reset.onclick = function () {
        tBody.innerHTML = ""
        party = 0

        allPlayers.forEach((item)=>{
            item.points = 0
            item.name = ""
        })

        allPlayers.forEach((item,index)=>{
            if(index % 2 === 0) {
                party=""
                tBody.insertAdjacentHTML("beforeend",`<tr><td rowspan="2">${party}</td><td>${item.name}</td><td>${item.points}</td></tr>`)
            }else {
                tBody.insertAdjacentHTML("beforeend",`<td>${item.name}</td><td>${item.points}</td></tr>`)
            }
        })

        allPlayers=[]
    }

    window.onresize = function () {
        ballPosition();
        racketPosition();
    } 

})


   /* switch(e.code) {y
            case "KeyW" : player1Racket.style.top = player1Racket.offsetTop - 10 + 'px'; break;
            case "KeyS" : player1Racket.style.top = player1Racket.offsetTop + 10 + 'px'; break;
            case "ArrowUp": player2Racket.style.top = player2Racket.offsetTop - 10 + 'px'; break;
            case "ArrowDown" : player2Racket.style.top = player2Racket.offsetTop + 10 + 'px'; break;
            
        } */ 





